package kz.mnc.anuarkaliyev23.checkers.coordinates;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ChessDeskBoardPositionTest {

    @Test
    void adjacentPositions() {
        ChessDeskBoardPosition positionB2 = new ChessDeskBoardPosition(ChessDeskVertical.B, ChessDeskHorizontal.II);
        List<BoardPosition> adjacentB2 = positionB2.adjacentPositions();
        System.out.println(adjacentB2);

        assertEquals(4, adjacentB2.size());
        assertTrue(adjacentB2.contains(new ChessDeskBoardPosition(ChessDeskVertical.A, ChessDeskHorizontal.I)));
        assertTrue(adjacentB2.contains(new ChessDeskBoardPosition(ChessDeskVertical.A, ChessDeskHorizontal.III)));
        assertTrue(adjacentB2.contains(new ChessDeskBoardPosition(ChessDeskVertical.C, ChessDeskHorizontal.I)));
        assertTrue(adjacentB2.contains(new ChessDeskBoardPosition(ChessDeskVertical.C, ChessDeskHorizontal.III)));
    }
}