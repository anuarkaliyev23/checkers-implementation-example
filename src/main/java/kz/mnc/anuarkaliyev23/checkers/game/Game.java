package kz.mnc.anuarkaliyev23.checkers.game;

import kz.mnc.anuarkaliyev23.checkers.coordinates.BoardPosition;
import kz.mnc.anuarkaliyev23.checkers.exception.CheckerNotFoundException;
import kz.mnc.anuarkaliyev23.checkers.move.Move;
import kz.mnc.anuarkaliyev23.checkers.piece.Checker;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Base class for checkers game
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public abstract class Game {
    /**
     * All checkers that are still in the game
     *
     * @since 0.1.0
     * */
    private List<Checker> checkers;

    /**
     * Flag signalling whose move
     * must be the next
     *
     * @since 0.1.0
     * */
    private boolean isWhiteMove;

    /**
     * Base method that is responsible for
     * handling of all the moves
     *
     * @since 0.1.0
     * */
    public abstract void handle(Move m);

    public Game(List<Checker> checkers) {
        this.checkers = checkers;
        isWhiteMove = true;
    }

    /**
     * Finds checker standing on position
     *
     * @param position - position on which checker stands on
     * @return checker standing on position if there is one
     * @throws CheckerNotFoundException if no checker is found
     *
     * @since 0.1.0
     * */
    public Checker findChecker(BoardPosition position) {
        for (Checker checker : checkers) {
            if (checker.getPosition().equals(position))
                return checker;
        }
        throw new CheckerNotFoundException(position);
    }

    /**
     * Finds checker standing on position or null otherwise
     *
     * @param position - position on which checker stands on
     * @return checker standing on position if there is one, null otherwise
     *
     * @since 0.1.0
     * */
    public Checker findCheckerOrNull(BoardPosition position) {
        for (Checker checker : checkers) {
            if (checker.getPosition().equals(position))
                return checker;
        }
        return null;
    }

    public List<Checker> getCheckers() {
        return checkers;
    }

    public void setCheckers(List<Checker> checkers) {
        this.checkers = checkers;
    }

    public boolean isWhiteMove() {
        return isWhiteMove;
    }

    public void setWhiteMove(boolean whiteMove) {
        isWhiteMove = whiteMove;
    }
}
