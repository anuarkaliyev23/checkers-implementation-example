package kz.mnc.anuarkaliyev23.checkers.game;

import kz.mnc.anuarkaliyev23.checkers.coordinates.BoardPosition;
import kz.mnc.anuarkaliyev23.checkers.exception.ImpossibleMoveException;
import kz.mnc.anuarkaliyev23.checkers.move.Move;
import kz.mnc.anuarkaliyev23.checkers.piece.Checker;
import kz.mnc.anuarkaliyev23.checkers.piece.KingChecker;

import java.util.ArrayList;
import java.util.List;

public class BasicGame extends Game {
    private Checker eatingSpreeChecker;

    public BasicGame(List<Checker> checkers) {
        super(checkers);
        eatingSpreeChecker = null;
    }


    //Checker that can be eaten MUST be eaten
    //That and only that Checker that eats it continues it's move
    @Override
    public void handle(Move m) {
        for (Checker checker : getCheckers()) {
            if (canBeEaten(checker) && checker.isWhite() != isWhiteMove()) {
                //Eat this checker!
            }
        }

        Checker checker = findChecker(m.getStart());

        if (checker.isWhite() != this.isWhiteMove())
            throw new ImpossibleMoveException(checker, m.getFinish());

        if (!checker.canMove(m.getFinish()))
            throw new ImpossibleMoveException(checker, m.getFinish());

        Checker finishChecker = findCheckerOrNull(m.getFinish());

        if (finishChecker != null) {
            if (finishChecker.isWhite() == checker.isWhite()) {
                throw new ImpossibleMoveException(checker, m.getFinish());
            }
            handleEating(m);
        }

        handleSimpleMove(m);
    }

    private boolean canBeEaten(Checker checker) {
        if (!getCheckers().contains(checker)) {
            throw new RuntimeException(String.format("Checker {%s} is not in the game", checker));
        }

        return canBeEatenBasic(checker) || canBeEatenKing(checker);
    }

    private boolean canBeEatenBasic(Checker checker) {
        if (!getCheckers().contains(checker)) {
            throw new RuntimeException(String.format("Checker {%s} is not in the game", checker));
        }
        List<BoardPosition> positions = checker.getPosition().adjacentPositions();
        List<Checker> adjacentCheckers = new ArrayList<>();

        for (BoardPosition position : positions) {
            Checker c = findCheckerOrNull(position);
            if (c != null)
                adjacentCheckers.add(c);
        }

        List<Checker> adjacentEnemies = new ArrayList<>();
        for (Checker c : adjacentCheckers) {
            if (c.isWhite() != checker.isWhite())
                adjacentEnemies.add(c);
        }
        if (adjacentEnemies.isEmpty()) {
            return false;
        }

        for (Checker enemy : adjacentEnemies) {
            List<BoardPosition> diagonals = enemy.getPosition().diagonals();
            List<BoardPosition> intersections = new ArrayList<>();

            for (BoardPosition dp : diagonals) {
                for (BoardPosition ap: positions) {
                    if (dp.equals(ap))
                        intersections.add(dp);
                }
            }
            if (intersections.isEmpty())
                return true;
        }
        return false;
    }

    private boolean canBeEatenKing(Checker checker) {
        if (!getCheckers().contains(checker)) {
            throw new RuntimeException(String.format("Checker {%s} is not in the game", checker));
        }

        List<KingChecker> enemyKings = new ArrayList<>();

        for (Checker c : getCheckers()) {
            if (c instanceof KingChecker && c.isWhite() != checker.isWhite()) {
                enemyKings.add((KingChecker) c);
            }
        }

        List<BoardPosition> adjacentPositions = checker.getPosition().adjacentPositions();

        for (Checker king : enemyKings) {
            List<BoardPosition> diagonals = king.getPosition().diagonals();
            List<BoardPosition> intersections = new ArrayList<>();

            for (BoardPosition diagonalPosition : diagonals) {
                for (BoardPosition adjacentPosition : adjacentPositions) {
                    if (diagonalPosition.equals(adjacentPosition)) {
                        intersections.add(adjacentPosition);
                    }
                }
            }

            if (intersections.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    private void handleEating(Move m) {
        if (eatingSpreeChecker != null) {
            if (findChecker(m.getStart()) != eatingSpreeChecker) {
                throw new ImpossibleMoveException(findChecker(m.getFinish()), m.getFinish());
            }
        }
    }

    private void handleSimpleMove(Move m) {

    }
}
