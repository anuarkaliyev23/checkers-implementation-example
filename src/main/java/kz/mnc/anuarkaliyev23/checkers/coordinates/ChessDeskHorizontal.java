package kz.mnc.anuarkaliyev23.checkers.coordinates;

import java.util.Arrays;

/**
 * Horizontal implementation of {@link BoardCoordinate} for
 * chessboard 8x8
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 */
public enum ChessDeskHorizontal implements BoardCoordinate{
    I(1),
    II(2),
    III(3),
    IV(4),
    V(5),
    VI(6),
    VII(7),
    VIII(8);

    private final int index;

    ChessDeskHorizontal(int index) {
        this.index = index;
    }

    @Override
    public int index() {
        return index;
    }

    @Override
    public BoardCoordinate next() {
        if (index == VIII.index) {
            return I;
        } else {
            return findByIndex(index + 1);
        }
    }

    @Override
    public BoardCoordinate previous() {
        if (index == I.index) {
            return VIII;
        } else {
            return findByIndex(index - 1);
        }
    }

    private static BoardCoordinate findByIndex(int index) {
        return Arrays.stream(values())
                .filter(coordinate -> coordinate.index == index)
                .findFirst()
                .get();
    }

}
