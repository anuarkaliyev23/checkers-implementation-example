package kz.mnc.anuarkaliyev23.checkers.coordinates;

import kz.mnc.anuarkaliyev23.checkers.exception.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link BoardPosition} extending class using {@link ChessDeskVertical} and {@link ChessDeskHorizontal} as
 * base for coordinates
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class ChessDeskBoardPosition extends BoardPosition {
    public ChessDeskBoardPosition(ChessDeskVertical vertical, ChessDeskHorizontal horizontal) {
        super(vertical, horizontal);
    }


    @Override
    public List<BoardPosition> adjacentPositions() {
        List<ChessDeskVertical> adjacentVerticals = new ArrayList<>();
        if (this.getVertical().index() != 0 && this.getVertical().index() != ChessDeskVertical.values().length) {
            adjacentVerticals.add((ChessDeskVertical) this.getVertical().next());
            adjacentVerticals.add((ChessDeskVertical) this.getVertical().previous());
        } else if (this.getVertical().index() == 0) {
            adjacentVerticals.add((ChessDeskVertical) this.getVertical().next());
        } else if (this.getVertical().index() == ChessDeskVertical.values().length) {
            adjacentVerticals.add((ChessDeskVertical) this.getVertical().previous());
        }

        List<ChessDeskHorizontal> adjacentHorizontals = new ArrayList<>();
        if (this.getHorizontal().index() != 0 && this.getHorizontal().index() != ChessDeskHorizontal.values().length) {
            adjacentHorizontals.add((ChessDeskHorizontal) this.getHorizontal().next());
            adjacentHorizontals.add((ChessDeskHorizontal) this.getHorizontal().previous());
        } else if (this.getVertical().index() == 0) {
            adjacentHorizontals.add((ChessDeskHorizontal) this.getHorizontal().next());
        } else if (this.getVertical().index() == ChessDeskVertical.values().length) {
            adjacentHorizontals.add((ChessDeskHorizontal) this.getVertical().previous());
        }

        List<BoardPosition> adjacentPositions = new ArrayList<>();

        for (ChessDeskVertical v : adjacentVerticals) {
            for (ChessDeskHorizontal h : adjacentHorizontals) {
                adjacentPositions.add(new ChessDeskBoardPosition(v, h));
            }
        }
        return adjacentPositions;
    }

    @Override
    public List<BoardPosition> diagonals() {
        int currentHorizontalIndex = this.getHorizontal().index();
        int currentVerticalIndex = this.getVertical().index();

        throw new NotImplementedException();
    }
}
