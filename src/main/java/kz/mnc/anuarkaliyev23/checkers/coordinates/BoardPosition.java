package kz.mnc.anuarkaliyev23.checkers.coordinates;

import java.util.List;
import java.util.Objects;

/**
 * Board Position represents 2 coordinates
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public abstract class BoardPosition {
    /**
     * {@link BoardCoordinate} representing vertical on the board
     *
     * @since  0.1.0
     * */
    private BoardCoordinate vertical;

    /**
     * {@link BoardCoordinate} representing horizontal on the board
     *
     * @since  0.1.0
     * */
    private BoardCoordinate horizontal;

    public BoardPosition(BoardCoordinate vertical, BoardCoordinate horizontal) {
        this.horizontal = horizontal;
        this.vertical = vertical;
    }

    public BoardCoordinate getHorizontal() {
        return horizontal;
    }

    public void setHorizontal(BoardCoordinate horizontal) {
        this.horizontal = horizontal;
    }

    public BoardCoordinate getVertical() {
        return vertical;
    }

    public void setVertical(BoardCoordinate vertical) {
        this.vertical = vertical;
    }

    abstract public List<BoardPosition> adjacentPositions();

    abstract public List<BoardPosition> diagonals();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardPosition that = (BoardPosition) o;
        return Objects.equals(horizontal, that.horizontal) && Objects.equals(vertical, that.vertical);
    }

    @Override
    public int hashCode() {
        return Objects.hash(horizontal, vertical);
    }

    @Override
    public String toString() {
        return "BoardPosition{" +
                "vertical=" + vertical +
                ", horizontal=" + horizontal +
                '}';
    }
}
