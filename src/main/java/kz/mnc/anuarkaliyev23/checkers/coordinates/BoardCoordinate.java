package kz.mnc.anuarkaliyev23.checkers.coordinates;

/**
 * Base interface for any coordinate
 * for checkers desk
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public interface BoardCoordinate {
    /**
     * @return int representation of the index
     * @since 0.1.0
     */
    int index();

    /**
     * @return next ordinal position for {@link BoardCoordinate}
     * @since 0.1.0
     * */
    BoardCoordinate next();

    /**
     * @return next ordinal position for {@link BoardCoordinate}
     * @since 0.1.0
     * */
    BoardCoordinate previous();

}
