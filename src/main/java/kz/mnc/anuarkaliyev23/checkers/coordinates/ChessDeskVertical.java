package kz.mnc.anuarkaliyev23.checkers.coordinates;

import java.util.Arrays;

/**
 * Vertical implementation of {@link BoardCoordinate} for
 * chessboard 8x8
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 */
public enum ChessDeskVertical implements BoardCoordinate {
    A(1),
    B(2),
    C(3),
    D(4),
    E(5),
    F(6),
    G(7),
    H(8);

    private final int index;
    ChessDeskVertical(int index) {
        this.index = index;
    }

    @Override
    public int index() {
        return index;
    }

    @Override
    public BoardCoordinate next() {
        if (index == H.index) {
            return A;
        } else {
            return findByIndex(index + 1);
        }
    }

    @Override
    public BoardCoordinate previous() {
        if (index == A.index) {
            return H;
        } else {
            return findByIndex(index - 1);
        }
    }

    private static BoardCoordinate findByIndex(int index) {
        return Arrays.stream(values())
                .filter(coordinate -> coordinate.index == index)
                .findFirst()
                .get();
    }
}
