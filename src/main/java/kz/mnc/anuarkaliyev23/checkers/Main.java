package kz.mnc.anuarkaliyev23.checkers;

import kz.mnc.anuarkaliyev23.checkers.game.BasicGame;
import kz.mnc.anuarkaliyev23.checkers.game.Game;
import kz.mnc.anuarkaliyev23.checkers.move.Move;
import kz.mnc.anuarkaliyev23.checkers.ui.ConsoleUI;
import kz.mnc.anuarkaliyev23.checkers.ui.UI;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Game game = new BasicGame(new ArrayList<>());
        UI ui = new ConsoleUI(new Scanner(System.in), game);

        ui.showDesk();

        while (true) {
            Move nextMove = ui.nextMove();
            game.handle(nextMove);
            ui.showDesk();
        }
    }
}
