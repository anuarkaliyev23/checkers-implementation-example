package kz.mnc.anuarkaliyev23.checkers.piece;

import kz.mnc.anuarkaliyev23.checkers.coordinates.BoardPosition;
import kz.mnc.anuarkaliyev23.checkers.exception.ImpossibleMoveException;

import java.util.Collections;
import java.util.List;

/**
 * Simple Checker implementation extending {@link Checker}.
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class BasicChecker extends Checker {
    public BasicChecker(BoardPosition position, boolean isWhite) {
        super(position, isWhite);
    }

    @Override
    public boolean canMove(BoardPosition target) {
        checkBoardPositionCompatibilityOrThrowException(target);
        int deltaVertical = target.getVertical().index() - this.getPosition().getVertical().index();
        int deltaHorizontal = target.getVertical().index() - this.getPosition().getVertical().index();
        if (Math.abs(deltaVertical) != ACCEPTABLE_ABSOLUTE_DELTA_VERTICAL)
            return false;

        if (isWhite()) {
            return deltaHorizontal == ACCEPTABLE_DELTA_HORIZONTAL_WHITE;
        } else {
            return deltaHorizontal == ACCEPTABLE_DELTA_HORIZONTAL_BLACK;
        }
    }

    @Override
    public List<BoardPosition> trace(BoardPosition target) throws ImpossibleMoveException  {
        checkBoardPositionCompatibilityOrThrowException(target);
        if (!canMove(target))
            throw new ImpossibleMoveException(this, target);
        return Collections.emptyList();
    }

    /**
     * This value represents acceptable difference between target posistion's cooridinate
     * and current position
     *
     * @since 0.1.0
     * */
    public static final int ACCEPTABLE_ABSOLUTE_DELTA_VERTICAL = 1;

    public static final int ACCEPTABLE_DELTA_HORIZONTAL_WHITE = 1;
    public static final int ACCEPTABLE_DELTA_HORIZONTAL_BLACK = -1;

}
