package kz.mnc.anuarkaliyev23.checkers.piece;

import kz.mnc.anuarkaliyev23.checkers.coordinates.BoardPosition;
import kz.mnc.anuarkaliyev23.checkers.exception.BoardPositionMismatchException;
import kz.mnc.anuarkaliyev23.checkers.exception.ImpossibleMoveException;

import java.util.List;
import java.util.Objects;

/**
 * Abstract class representing checker
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public abstract class Checker {
    /**
     * Checker's position on the board
     *
     * @since 0.1.0
     * */
    private BoardPosition position;

    /**
     * boolean flag representing if checker is white
     *
     * @since 0.1.0
     * */
    private boolean isWhite;

    public Checker(BoardPosition position, boolean isWhite) {
        this.position = position;
        this.isWhite = isWhite;
    }

    public BoardPosition getPosition() {
        return position;
    }

    public void setPosition(BoardPosition position) {
        this.position = position;
    }

    public boolean isWhite() {
        return isWhite;
    }

    public void setWhite(boolean white) {
        isWhite = white;
    }

    /**
     * Function representing if checker can move onto target position.
     * This function is intended to be used as validity check if some position
     * can be valid theoretically.
     * This function is used as starter check. If this function returns false that would mean
     * that move is even theoretically invalid. If this function returns true that means that move
     * is possible to be correct. However this function don't check any positional parameters, other
     * checkers etc.
     *
     * @param target - BoardPosition on which checker might move;
     * @return true if target can be theoretically valid, false if there is no any chance for move to be valid
     * */
    public abstract boolean canMove(BoardPosition target);

    /**
     * Function returning all positions between current {@link this#position} and target position
     *
     * @param target - BoardPosition on which checker might move;
     * @return all positions between current {@link this#position} and target position, excluding them from final list
     * */
    public abstract List<BoardPosition> trace(BoardPosition target) throws ImpossibleMoveException;

    protected void checkBoardPositionCompatibilityOrThrowException(BoardPosition target) throws BoardPositionMismatchException {
        if (target.getClass() != position.getClass()) {
            throw new BoardPositionMismatchException(target, position.getClass());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Checker checker = (Checker) o;
        return isWhite == checker.isWhite && Objects.equals(position, checker.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, isWhite);
    }

    @Override
    public String toString() {
        return "Checker{" +
                "position=" + position +
                ", isWhite=" + isWhite +
                '}';
    }
}
