package kz.mnc.anuarkaliyev23.checkers.piece;

import kz.mnc.anuarkaliyev23.checkers.coordinates.BoardPosition;
import kz.mnc.anuarkaliyev23.checkers.exception.ImpossibleMoveException;
import kz.mnc.anuarkaliyev23.checkers.exception.NotImplementedException;

import java.util.List;

public class KingChecker extends Checker {
    public KingChecker(BoardPosition position, boolean isWhite) {
        super(position, isWhite);
    }

    @Override
    public boolean canMove(BoardPosition target) {
        int deltaVertical = target.getVertical().index() - this.getPosition().getVertical().index();
        int deltaHorizontal = target.getHorizontal().index() - this.getPosition().getVertical().index();

        return (Math.abs(deltaHorizontal) == Math.abs(deltaVertical) && deltaHorizontal > 0);
    }

    @Override
    public List<BoardPosition> trace(BoardPosition target) throws ImpossibleMoveException {
        throw new NotImplementedException();
    }


}
