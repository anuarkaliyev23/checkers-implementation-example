package kz.mnc.anuarkaliyev23.checkers.move;

import kz.mnc.anuarkaliyev23.checkers.coordinates.BoardPosition;

import java.util.Objects;

/**
 * Base class for all possible moves
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class Move {
    private BoardPosition start;
    private BoardPosition finish;

    public Move(BoardPosition start, BoardPosition finish) {
        this.start = start;
        this.finish = finish;
    }

    public BoardPosition getStart() {
        return start;
    }

    public void setStart(BoardPosition start) {
        this.start = start;
    }

    public BoardPosition getFinish() {
        return finish;
    }

    public void setFinish(BoardPosition finish) {
        this.finish = finish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Move move = (Move) o;
        return Objects.equals(start, move.start) && Objects.equals(finish, move.finish);
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, finish);
    }

    @Override
    public String toString() {
        return "Move{" +
                "start=" + start +
                ", finish=" + finish +
                '}';
    }
}
