package kz.mnc.anuarkaliyev23.checkers.ui;


import kz.mnc.anuarkaliyev23.checkers.move.Move;

/**
 * Base interface for all user-related interaction
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 */
public interface UI {

    /**
     * Method that shows desk to end-user
     *
     * @since 0.1.0
     */
    void showDesk();


    /**
     * Method returning next move for checkers game
     *
     * @return next move to be made
     * @since 0.1.0
     */
    Move nextMove();

}
