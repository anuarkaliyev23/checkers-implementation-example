package kz.mnc.anuarkaliyev23.checkers.ui;

import kz.mnc.anuarkaliyev23.checkers.game.BasicGame;
import kz.mnc.anuarkaliyev23.checkers.game.Game;
import kz.mnc.anuarkaliyev23.checkers.move.Move;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Basic implementation of the UI interface
 * for computer terminal
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class ConsoleUI implements UI {
    private Scanner scanner;
    private Game game;

    public ConsoleUI(Scanner scanner, Game game) {
        this.scanner = scanner;
        this.game = game;
    }

    @Override
    public void showDesk() {

    }

    @Override
    public Move nextMove() {
        return null;
    }
}
