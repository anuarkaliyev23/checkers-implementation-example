package kz.mnc.anuarkaliyev23.checkers.exception;

import kz.mnc.anuarkaliyev23.checkers.coordinates.BoardPosition;
import kz.mnc.anuarkaliyev23.checkers.piece.Checker;

/**
 * Unchecked exception that should be thrown in the case of the impossible move on the desk
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class ImpossibleMoveException extends RuntimeException {
    /**
     * Checker which was found impossible to move
     *
     * @since 0.1.0
     * */
    private Checker checker;

    /**
     * Position on which {@link ImpossibleMoveException#checker} was trying to move
     *
     * @since 0.1.0
     * */
    private BoardPosition targetPosition;

    public ImpossibleMoveException(Checker checker, BoardPosition targetPosition) {
        super(String.format("Checker {%s} was found impossible to be moved on position {%s}", checker, targetPosition));
        this.checker = checker;
        this.targetPosition = targetPosition;
    }

    public ImpossibleMoveException(Checker checker, BoardPosition targetPosition, Throwable cause) {
        super(String.format("Checker {%s} was found impossible to be moved on position {%s}", checker, targetPosition), cause);
        this.checker = checker;
        this.targetPosition = targetPosition;
    }

    public Checker getChecker() {
        return checker;
    }

    public BoardPosition getTargetPosition() {
        return targetPosition;
    }
}
