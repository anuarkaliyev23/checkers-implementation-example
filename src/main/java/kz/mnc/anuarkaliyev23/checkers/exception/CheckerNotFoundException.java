package kz.mnc.anuarkaliyev23.checkers.exception;

import kz.mnc.anuarkaliyev23.checkers.coordinates.BoardPosition;

public class CheckerNotFoundException extends RuntimeException {
    private BoardPosition coordinate;

    public CheckerNotFoundException(BoardPosition coordinate) {
        super("Could not find a checker at cooridinate " + coordinate);
        this.coordinate = coordinate;
    }

    public CheckerNotFoundException(Throwable cause, BoardPosition coordinate) {
        super(cause);
        this.coordinate = coordinate;
    }
}
