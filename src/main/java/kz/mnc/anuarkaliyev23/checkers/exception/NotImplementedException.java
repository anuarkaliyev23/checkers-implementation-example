package kz.mnc.anuarkaliyev23.checkers.exception;

/**
 * Should be thrown if some method still has no
 * valid implementation
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class NotImplementedException extends RuntimeException {
    public NotImplementedException() {
        super();
    }

    public NotImplementedException(String message) {
        super(message);
    }

    public NotImplementedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotImplementedException(Throwable cause) {
        super(cause);
    }

    protected NotImplementedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
