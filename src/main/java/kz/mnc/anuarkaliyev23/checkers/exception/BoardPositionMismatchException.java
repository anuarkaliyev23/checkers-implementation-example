package kz.mnc.anuarkaliyev23.checkers.exception;

import kz.mnc.anuarkaliyev23.checkers.coordinates.BoardPosition;

/**
 * Unchecked Exception that should be thrown if some {@link BoardPositionMismatchException#position}
 * is not compatible with {@link BoardPositionMismatchException#sourcePositionClass}
 *
 * @since 0.1.0
 * @author Khambar Dussaliyev
 * */
public class BoardPositionMismatchException extends RuntimeException {
    private Class<? extends BoardPosition> sourcePositionClass;
    private BoardPosition position;

    public BoardPositionMismatchException(BoardPosition position, Class<? extends BoardPosition> sourcePosition) {
        super(String.format("Position {%s} is mismatched with {%s}", position, sourcePosition));
        this.position = position;
        this.sourcePositionClass = sourcePosition;
    }

    public BoardPositionMismatchException(Class<? extends BoardPosition> sourcePosition, BoardPosition position, Throwable cause) {
        super(cause);
        this.sourcePositionClass = sourcePosition;
        this.position = position;
    }

    public Class<? extends BoardPosition> getSourcePositionClass() {
        return sourcePositionClass;
    }

    public BoardPosition getPosition() {
        return position;
    }
}
